require('./app-global');
const express = require(`${__config}/express`),
 config = require(`${__config}/config`),
 server = require('http').Server(express);
//import db 
//require(`${__config}/sqlDbConn`);
server.listen(`${config.port}`);
console.log(`Server is listing on port ${config.port}`)