global.__root = `${__dirname}`;
global.__base = `${__root}/src`;
global.__config = `${__base}/config`;
global.__routes = `${__base}/routes`;
global.__controller = `${__base}/controller`;
global.__uploads = `${__root}/public/images`;
global.__models = `${__base}/models`