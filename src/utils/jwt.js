const jwt = require('jsonwebtoken');
const config = require(`${__config}/config`);
exports.createJwtToken = async(obj) => {
    try {
        let data = {
            time: Date(),
            username: obj.email,
            isAdmin: obj.isAdmin
        }
        let jwtSecretKey = config.secretTokenKey;
        let token = jwt.sign(data, jwtSecretKey, {
            expiresIn: 3600
        });
        return token
    } catch (error) {
        return error
    }
}

exports.verifyToken = async(req, res, next) => {
    try {
        let token = req.headers.authorization;
        //token = token.split(" ")[1];
        let resl;
        jwt.verify(token, config.secretTokenKey, (err, decoded) => {
            if (err) {
                return res.send({ success: false, status: -100, message: "Sorry token is invalid" })
            }
            if (decoded.isAdmin !== Number(req.headers.isadmin)) {
                return res.send({ success: false, status: -100, message: "Sorry! You are not authorize for this " })
            }
            resl = decoded;
        });
        req.body.username = resl.username;
        next();
    } catch (error) {
        throw error;
    }
}