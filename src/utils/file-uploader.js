
const multer = require("multer");
const path = require("path");
const directoryPath = path.join(__root, "/public/images");

var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    console.log("multer called");

    cb(null, directoryPath);
  },
  filename: (req, file, cb) => {
    let imgName;
    if(req.body.name){
        imgName = req.body.name;
    }else{
        imgName = 'banner'
    }
    cb(
      null,
      file.fieldname + path.extname(file.originalname),
    );
  },
});
const upload = multer({storage:storage});
exports.upload = upload;