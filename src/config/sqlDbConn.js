const mysql = require('mariadb');
const config = require('./config');
const util = require('util');
const pool = mysql.createPool({
    host: config.sql_db.host,
    user: config.sql_db.username,
    password: config.sql_db.password,
    database:config.sql_db.database,
    connectionLimit:5
});
(async ()=>{
    
    
})()


exports.query = async (query) => {
    let connection;
    let promise = await new Promise(async (resolve, reject) => {
      connection = await pool.getConnection();
      console.log("Query: ", query);
      try {
        const res = await connection.query(query, [1, "mariadb"]);
      
        connection.end();
        resolve(res);
      } catch (error) {
        console.log("errr: ", error);
        connection.end();
  
        reject(error);
      }
    });
  
    return promise;
  };
