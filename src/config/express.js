const express = require('express'),
    app = express(),
    cors = require('cors'),
    cookieParser = require('cookie-parser'),
    { verifyToken } = require('../utils/jwt'),
    indexRouter = require(`${__routes}/`);
mongoDb = require(`${__config}/dbConn`);
app.use(cors());
app.use(express.json({ limit: '20mb' }));
app.use(express.urlencoded({ extended: true | false }));
app.use(cookieParser());
var publicDir = require('path').join(`${__root}`, '/public/images');
app.use('/images', express.static(publicDir));
app.use((req, res, next) => {
    if (!["POST", "GET", "DELETE", "PUT", "PATCH"].includes(req.method)) res.status(403).json({ "message": "Mehtod Not allowed" });
    next();
    if (req.url !== '/login' && !req.headers.authorization) {
        res.send({ success: false, message: 'Invalid Request' });
    }

});
app.use('/api/v1', indexRouter)
module.exports = app;