const mongoose = require("mongoose");
mongoose.Promise = require("bluebird");
const config = require("./config");

function getDatabaseUrl() {
  return (
    config.db_config.driver +
    "://" +
    config.db_config.host +
    ":" +
    config.db_config.port +
    "/" +
    config.db_config.dbName
  );
}

const dbURI = getDatabaseUrl();
const connectWithRetry = function () {
  return mongoose.connect(dbURI, function (err) {
    if (err) console.error(".mongoose.connect failed to connect", err);
  });
};

connectWithRetry();

mongoose.connection.on("connected", function () {
  console.info(
    "----Database " + dbURI+ " connection has been established with"
  );
});

mongoose.connection.on("error", function (err) {
  console.error(".mongoose.error, connection to mongo failed", err);
});

mongoose.connection.on("disconnected", function () {
  console.warn(".mongoose.disconnected connection closed - retrying in 5 sec");
  setTimeout(connectWithRetry, 5000);
});
