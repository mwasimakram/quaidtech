const router = require('express').Router(),
      userCtrl = require(`${__controller}/user`);

require(`${__routes}/user`)(router,userCtrl);

module.exports = router;