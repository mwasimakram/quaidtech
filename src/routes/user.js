const { upload } = require('../utils/file-uploader');
const { verifyToken } = require('../utils/jwt');
const { checkUserReg } = require('../middelwares/user');
module.exports = (router, controller) => {
    router.post('/register', checkUserReg, controller.registerUser);
}