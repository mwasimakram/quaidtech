const _ = require('underscore');
const { createJwtToken } = require('../utils/jwt');
const fs = require('fs');
const md5 = require('md5');
const User = require(`${__models}/User`);
// exports.loginUser = async(req, res) => {
//     try {
//         const { email, password } = req.body;
//         if (!email && !password) {
//             return res.status(401).json({ success: false, message: "Email and password are required" });
//         }
//         let result = await query(`SELECT * FROM users WHERE email='${email}' AND password= md5('${password}')`);
//         if (result.code) {
//             return res.status(401).json({ success: false, message: "Sorry something is wrong with sql query" });
//         }
//         result = _.difference(result, "meta");
//         if (result.length == 0) return res.json({ success: false, message: "Sorry username or password is wrong" });
//         let token = await createJwtToken(result[0]);
//         res.json({ success: true, token: token, isAdmin: result[0].isAdmin });
//     } catch (error) {
//         res.status(405).json({ success: false, error: error });
//     }
// }

exports.registerUser = async(req, res) => {
    try {
        const { name, password, email } = req.body;
        const userObj = {
            name: name,
            password: password,
            email: email
        };
        const user = new User(userObj);
        const result = await user.save();
        res.send({ success: true, data: result });
    } catch (error) {
        res.send({ success: false, messages: error.message });
    }
}