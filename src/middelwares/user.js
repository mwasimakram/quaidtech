exports.checkUserReg = async(req, res, next) => {
    try {
        let messages = [];
        const { name, email, password } = req.body;
        if (!name || !email || !password) {
            messages.push("Invalid data")
        }
        if (email) {
            let isValidEmail = await checkEmail(email);
            if (!isValidEmail) {
                messages.push("Please enter a valid email");
            }
        }
        if (name.length < 6) {
            messages.push("Name should be atleast 6 char logn");
        }
        if (name.length >= 6) {
            let result = await checkForUpperAndLowerCase(name);
            console.log(result);
            if (!result) {
                messages.push("Name should contain atleast one uppercase and one lowercase character");
            }
        }
        if (messages.length > 0) {
            return res.send({ success: false, messages: messages });
        }
        next();
    } catch (error) {
        res.send({ success: false, message: error });
    }
}

async function checkForUpperAndLowerCase(str) {
    return /[a-z]/.test(str) && /[A-Z]/.test(str);
}
async function checkEmail(email) {
    return String(email)
        .toLowerCase()
        .match(
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        );
}