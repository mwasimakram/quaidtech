const user = require('../routes/user');

const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    { isEmail } = require('validator');
const userSchema = new Schema({
    name: {
        type: String,
        required: true,
        minlength: 6,

    },
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true,
        validate: [isEmail, 'Invalid Email']
    }
});
module.exports = new mongoose.model("Users", userSchema);
console.log("User Schema created Successfully");